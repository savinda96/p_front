import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../user';
import{Injury} from '../user';
import{Ar} from '../user';
import{Re} from '../user';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    register(user: User) {
      debugger;
        return this.http.post(`http://localhost:4000/users/register`, user);
    }

    getAllTreat() {
      return this.http.get<Injury[]>(`http://localhost:4000/users/treatment`);
  }

    getAllAr(_id){
      console.log(_id);
      return this.http.get<Ar[]>(`http://localhost:4000/users/ar/`+_id);

    }

    getAllRe(_id){
      console.log(_id);
      return this.http.get<Re[]>(`http://localhost:4000/users/re/`+_id);

    }
    
    loadtreat(_id){
      return this.http.get<Injury[]>(`http://localhost:4000/users/loadt/`+_id);
    }

}
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AlertService,UserService} from '../services'
import { Injury } from '../user';


@Component({
  selector: 'app-signupPatient',
  templateUrl: './signupPatient.component.html',
  styleUrls: ['./signupPatient.component.scss']
})
export class SignupPatientComponent implements OnInit {

    data : Date = new Date();
    error:string;
    injurys : any[];
    registerForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private alertService: AlertService,
        private userService: UserService) { }

    ngOnInit() {

        this.registerForm = this.formBuilder.group({
            firstName: ['', [Validators.required,Validators.pattern('^[a-zA-Z\s]*$')]],
            lastName: ['', [Validators.required,Validators.pattern('^[a-zA-Z\s]*$')]],
            gender: ['',Validators.required],
            contactNo: ['', [Validators.required]],
            username: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            therapist:['', [Validators.required]],
            injury:['', [Validators.required]]
        });

        this.loadAlltreatments();
    
        



        var body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');

        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');
    }
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        this.alertService.clear();



        // stop here if form is invalid
        if (this.registerForm.invalid) {
            debugger
            return;
        }

        
        this.loading = true;
        /* this.alertService.success('Registration successful', true);
        this.router.navigate(['/index']); */
        

        this.userService.register(this.registerForm.value)
             .pipe(first())
             .subscribe(
                 data => {
                    this.router.navigate(['/index'], { queryParams: { registered: true }});
                 },
                 error => {
                     this.error=error.error.message;
                     console.log(error.message);
                     this.loading = false;
                 }); 

        
    }

    btnClick= function () {
        this.router.navigateByUrl('/index');
    };

    private loadAlltreatments() {
        this.userService.getAllTreat()
            .subscribe(data => this.injurys = data);
    }


}

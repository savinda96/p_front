import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { AgmCoreModule } from '@agm/core';

import {SignupPatientComponent} from './signupPatient/signupPatient.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { ExamplesComponent } from './examples.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { AlertService, UserService, AuthenticationService } from './services';
import { AlertComponent } from './alert/alert.component';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Jwtinterceptor } from './services';



@NgModule({
    imports: [
        CommonModule,
        NgCircleProgressModule.forRoot({
            // set defaults here
            radius: 80,
            outerStrokeWidth: 16,
            innerStrokeWidth: 8,
            outerStrokeColor: "#78C000",
            innerStrokeColor: "#C7E596",
            animationDuration: 300,
          }),
        FormsModule,
        NgbModule,
        NouisliderModule,
        ReactiveFormsModule,
        JwBootstrapSwitchNg2Module,
        AgmCoreModule.forRoot({
            apiKey: 'YOUR_KEY_HERE'
        }),
        HttpClientModule
    ],
    declarations: [
        LoginComponent,
        ExamplesComponent,
        ProfileComponent,
        SignupPatientComponent,
        AlertComponent
    ],
    providers: [AlertService,UserService,AuthenticationService,{provide: HTTP_INTERCEPTORS,
        useClass: Jwtinterceptor,
        multi: true}]
})
export class ExamplesModule { }

export class User {
    _id: number;
    firstName: string;
    lastName: string;
    gender:string;
    contactNo: string;
    username: string;
    password: string;
    therapist: string;
    injury: string;
    token: string;
}
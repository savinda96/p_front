import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule,CanActivate } from '@angular/router';
import { 
    AuthGuardService as AuthGuard 
  } from './examples/services';

import { ComponentsComponent } from './components/components.component';
import { LoginComponent } from './examples/login/login.component';
import {SignupPatientComponent} from './examples/signupPatient/signupPatient.component';
import { ProfileComponent } from './examples/profile/profile.component';
import { NucleoiconsComponent } from './components/nucleoicons/nucleoicons.component';
import { from } from 'rxjs';

const routes: Routes =[
    { path: '', redirectTo: 'index', pathMatch: 'full' },
    { path: 'index',                component: LoginComponent },
    { path: 'nucleoicons',          component: NucleoiconsComponent },
    { path: 'components',       component: ComponentsComponent  },
    { path: 'examples/profile',     component: ProfileComponent,canActivate:[AuthGuard] },
    { path: 'examples/signupPatient',     component: SignupPatientComponent }
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
    ],
})
export class AppRoutingModule { }
